package server;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;




@RestController
public class ServerResponseControler  {

    private BookSearchFunctions bookSearchFunctions;
    private ObjectMapper mapper;
    

    // Endpoint for searching by isbn/id    
    @Cacheable("IsbnSearch")
    @RequestMapping(value = "api/book/{isbnToFind}" , method = RequestMethod.GET, produces="application/json")    
    public String findIsbn(@PathVariable String isbnToFind){
        bookSearchFunctions = new BookSearchFunctions();
        String preetyJsonReposne=null;        
        BookInfoPojo bookFoundByIsbn=bookSearchFunctions.searchByIsbn(isbnToFind);
        if(bookFoundByIsbn==null){
            throw new ResponseStatusException( HttpStatus.NOT_FOUND, "book not found");
        }
        mapper = new ObjectMapper();
        try{
        preetyJsonReposne = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookFoundByIsbn);
        }catch(Exception e){ }

        
        return preetyJsonReposne;
        }
        
    

    // Endpoint for find all books belonging to category
    @Cacheable("Category")
    @RequestMapping(value = "api/{categoryToFind}/books" , method = RequestMethod.GET, produces="application/json")    
    public String findCategory(@PathVariable String categoryToFind){
        String preetyJsonReposne=null;
        mapper = new ObjectMapper();
        bookSearchFunctions = new BookSearchFunctions();
        List<BookInfoPojo> listOfBooksFoundByCategory=bookSearchFunctions.searchByCategory(categoryToFind);
        
        try{
            preetyJsonReposne = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listOfBooksFoundByCategory);
        }catch(Exception e){ }
        return preetyJsonReposne;
        
        
    }


    // Endpoint for searching by query
    @Cacheable("SearchQuery")
    @RequestMapping(value = "api/search" , method = RequestMethod.GET, produces="application/json")    
    public String findQuery(@RequestParam String queryToFind){
        String preetyJsonReposne=null;
        mapper = new ObjectMapper();
        bookSearchFunctions = new BookSearchFunctions();
        List<BookInfoPojo> listOfBooksFoundByCategory=bookSearchFunctions.searchByQuery(queryToFind);
        if(listOfBooksFoundByCategory==null){
        
            throw new ResponseStatusException( HttpStatus.NOT_FOUND, "query not found" );
           
        }
        else{
            try{
                preetyJsonReposne = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listOfBooksFoundByCategory);
                }catch(Exception e){
    
                }
            return preetyJsonReposne;
        }
        
    }


    
    // Endpoint for pulling all authors and thier average rating
    @Cacheable("Rating")
    @RequestMapping(value = "api/rating" , method = RequestMethod.GET, produces="application/json")    
    public String sortAuthorsRating(){
        String preetyJsonReposne=null;
        mapper = new ObjectMapper();
        bookSearchFunctions = new BookSearchFunctions();
        List<AuthorsRatingPojo> listOfSortedAuthorsRating=bookSearchFunctions.sortedListOfAuthorsRating();
            
            
        try{
            preetyJsonReposne = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listOfSortedAuthorsRating);
        }catch(Exception e){}
               
        return preetyJsonReposne;
            
            
        }


        
        

    

}