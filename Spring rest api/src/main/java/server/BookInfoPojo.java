package server;
import java.util.List; 
import java.lang.Integer;
import com.fasterxml.jackson.annotation.JsonInclude;

// Pojo for Books Info, using Include.Non_Empty so that id a value is null it will not be send by Json
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BookInfoPojo{
    private String isbn;
    private String title;
    private String subtitle;
    private String publisher;
    private Long publishedDate;
    private String description;
    private Integer pageCount;
    private String thumbnailUrl;
    private String language;
    private String previewLink;
    private String averageRating;
    private List<String> authors;
    private List<String> categories;


    
    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }
    
    public String getSubtitle() {
        return subtitle;
    }

    public String getPublisher() {
        return publisher;
    }

    public Long getPublishedDate() {
        return publishedDate;
    }


    public String getDescription() {
        return description;
    }


    public Integer getPageCount() {
        return pageCount;
    }

  
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getLanguage() {
        return language;
    }


    public String getPreviewLink() {
        return previewLink;
    }


    public String getAverageRating() {
        return averageRating;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public String getAuthors(int index) {
        return authors.get(index);
    }
    public List<String> getCategories() {
        return categories;
    }



}