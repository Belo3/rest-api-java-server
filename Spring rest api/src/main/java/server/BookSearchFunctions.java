package server;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import java.io.File;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.regex.Pattern;

import java.util.regex.Matcher;

public class BookSearchFunctions{

    // List Used in all functions, Is created at the beggining of Main
    private static List<BookInfoPojo> createdJsonListOfBooks;

    // Since my test bypasses the usage of main, I had to creat a ifThisIsTest variable to set to true for reading from 
    // prepared sets of data. At the begging of every function it will call createJsonList    
    public static boolean ifThisIsTest = false;
    
    // Reads out file, and throws a IOException
    private JsonNode readJson(){
        JsonNode jsonNodeOfListOfBooks = null;
        ObjectMapper objectMapper = new ObjectMapper();

        if(ifThisIsTest){
            try{
                Path currentDir = Paths.get("JsonTestObject.json");
                String patchToTestJson = currentDir.toAbsolutePath().toString();
                JsonNode rootNode = objectMapper.readTree(new File(patchToTestJson));
                jsonNodeOfListOfBooks = rootNode.path("items");
            }catch(IOException e){
                System.err.println("Error Input Test file");
            }
        }

        else{
            try{
                JsonNode rootNode = objectMapper.readTree(new File(System.getProperty("datasource")));
                jsonNodeOfListOfBooks = rootNode.path("items");
            }catch(IOException e){
                System.err.println("Error Input file");
        }
        }
        return jsonNodeOfListOfBooks;
    }



    // Using readJson(), creats a lists of Pojo objects with info about books, allowing for further proccesing of them
    public void creatJsonList(){
        JsonNode jsonBookInformation = readJson();
        List<BookInfoPojo> listOfBooksPojo = new ArrayList<>();
        String isbn=null;
        BookInfoPojo bookInfoPojo;
        ObjectMapper mapper = new ObjectMapper();
        
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        // Iterating through all books to gather info for BooksInfoPojo
        for(JsonNode root : jsonBookInformation){            
            
            ObjectNode objectNode = (ObjectNode) root.path("volumeInfo");
            Boolean checkIfIsbn_13 = false;
            bookInfoPojo=null;

            // Changes date to UNIX value, assuming its present
            String dateString = root.path("volumeInfo").path("publishedDate").asText();            
            if(!dateString.equals("")){                    
            objectNode.put("publishedDate",stringFormateToDate(dateString));
            }

            for(JsonNode node : root.path("volumeInfo").path("industryIdentifiers"))
            {
                // Looking for a ISBN_13 Id, if found adding book info to list of books
                if(node.path("type").toString().equals("\"ISBN_13\"") ){                    
                    isbn=node.path("identifier").asText();
                    try{                

                    
                    bookInfoPojo = mapper.treeToValue(root.path("volumeInfo"), BookInfoPojo.class);                
                    bookInfoPojo.setIsbn(isbn);
                    bookInfoPojo.setThumbnailUrl(root.path("volumeInfo").path("imageLinks").path("thumbnail").asText());
                    listOfBooksPojo.add(bookInfoPojo);
                    }catch(Exception e){
                        System.out.println(e);
                        
                    }

                    checkIfIsbn_13 = true;
                }
            }
        if(!checkIfIsbn_13){
            
            // If ISBN_13 is not present, using id as an proper value, and adding book info to list of books
            isbn=root.path("id").asText();

            
            try {
                bookInfoPojo = mapper.treeToValue(root.path("volumeInfo"), BookInfoPojo.class);
                
                bookInfoPojo.setIsbn(isbn);
                bookInfoPojo.setThumbnailUrl(root.path("volumeInfo").path("imageLinks").path("thumbnail").asText());
                listOfBooksPojo.add(bookInfoPojo);

            } catch (Exception e) {
            }            
            }        
    }
    createdJsonListOfBooks = listOfBooksPojo;
}


    // Formats date to unix value, trying to first convert by "yyyy-MM-dd" format, on fail with "yyyy" format
    private Double stringFormateToDate(String dateToFormat){
    double dateInUnix=0.0;
    Date dateString;
    try{
    DateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
    dateString = formater.parse(dateToFormat);
    dateInUnix = dateString.getTime()/1000L;
    }catch(Exception e){
        try {
            DateFormat formater = new SimpleDateFormat("yyyy");
            dateString = formater.parse(dateToFormat);
            dateInUnix = dateString.getTime()/1000L;
        } catch (Exception i) {
            
        }
    }
    return dateInUnix;
}   



    // Function that searches for book by ISBN from list of all books
    public BookInfoPojo searchByIsbn(String isbnToFind){
        if(ifThisIsTest)
        creatJsonList();
        BookInfoPojo foundBook=null;
        List<BookInfoPojo> listOfBooksPojo = createdJsonListOfBooks;
        for(int i=0; listOfBooksPojo.size() > i; i++){
            if(listOfBooksPojo.get(i).getIsbn().equals(isbnToFind)){
                foundBook=listOfBooksPojo.get(i);   
            }
            }
        return foundBook;
}




    // Function that searches for books by matching category 
    public List<BookInfoPojo> searchByCategory(String categoryToFind){
    if(ifThisIsTest)
    creatJsonList();
    List<BookInfoPojo> foundBooks=new ArrayList<>();
    List<BookInfoPojo> listOfBooksPojo = createdJsonListOfBooks;
    for(int indexOfBook=0; listOfBooksPojo.size() > indexOfBook; indexOfBook++){
        // Checking if category is present
        if(!(listOfBooksPojo.get(indexOfBook).getCategories()==null)){
        for(int indexOfCategory=0; listOfBooksPojo.get(indexOfBook).getCategories().size() > indexOfCategory; indexOfCategory++){
        // Cheking if any of categories is matching
        if(listOfBooksPojo.get(indexOfBook).getCategories().get(indexOfCategory).equalsIgnoreCase(categoryToFind)){
            foundBooks.add(listOfBooksPojo.get(indexOfBook));
            }
        }
    }
}
    return foundBooks;
}


    // Functions that searches for query
    public List<BookInfoPojo> searchByQuery(String queryToFind){
    if(ifThisIsTest)
    creatJsonList();
    List<BookInfoPojo> foundBooks=new ArrayList<>();
    List<BookInfoPojo> listOfBooksPojo = createdJsonListOfBooks;
    String searchQuery;
    Pattern queryPattern;
    Matcher queryMatcher;
    
    // Adds a markes for patters, it will search for all words separeted by " " in any order, but all the words provided have to be present
    // All of words have to be in one of the properties
	String[] querySplittedToWords=queryToFind.split(" ");
    for (int indexOfQuery=0; querySplittedToWords.length > indexOfQuery; indexOfQuery++) {
        querySplittedToWords[indexOfQuery] = "(?i)(?=.*?" + querySplittedToWords[indexOfQuery] + ")";
    }
    // Joining splitted query
    searchQuery = String.join("", querySplittedToWords);
    queryPattern = Pattern.compile(searchQuery);


    // I have chosen to look for query only in decryption, title, subtitle and publisher, but ofcourse i would be easy to look elsewhere as well
    for(int indexOfBook=0; listOfBooksPojo.size() > indexOfBook; indexOfBook++){
        boolean valueIfFound = true;


        // Looking for pattern in Descryption, if present
        if(!(listOfBooksPojo.get(indexOfBook).getDescription() == null) & valueIfFound){
            String stringToCheck =listOfBooksPojo.get(indexOfBook).getDescription();
            queryMatcher = queryPattern.matcher(stringToCheck);
            if(queryMatcher.find()){
                foundBooks.add(listOfBooksPojo.get(indexOfBook));
                valueIfFound = false;
            }
            
        }


        // Looking for pattern in Title, if present
        if(!(listOfBooksPojo.get(indexOfBook).getTitle() == null) & valueIfFound){
            String stringToCheck =listOfBooksPojo.get(indexOfBook).getTitle();
            queryMatcher = queryPattern.matcher(stringToCheck);
            if(queryMatcher.find()){
                foundBooks.add(listOfBooksPojo.get(indexOfBook));
                valueIfFound = false;
            }
            
        }


        // Looking for pattern in Subtitle, if present
        if(!(listOfBooksPojo.get(indexOfBook).getSubtitle() == null) & valueIfFound){
            String stringToCheck =listOfBooksPojo.get(indexOfBook).getSubtitle();
            queryMatcher = queryPattern.matcher(stringToCheck);
            if(queryMatcher.find()){
                foundBooks.add(listOfBooksPojo.get(indexOfBook));
                valueIfFound = false;
            }
            
        }


        // Looking for pattern in Publisher, if present
        if(!(listOfBooksPojo.get(indexOfBook).getPublisher() == null) & valueIfFound){
            String stringToCheck =listOfBooksPojo.get(indexOfBook).getPublisher();
            queryMatcher = queryPattern.matcher(stringToCheck);
            if(queryMatcher.find()){
                foundBooks.add(listOfBooksPojo.get(indexOfBook));
                valueIfFound = false;
            }
            
        }

    }

    return foundBooks;
}


   

    // Function that gathers all authors and thier rating, and returns sorted Array of AuthorsRatingPojo, Authors with no rating are at the bottom
    public List<AuthorsRatingPojo> sortedListOfAuthorsRating(){
        if(ifThisIsTest)
        creatJsonList();
        List<AuthorsRatingPojo> sortedListOfAuthors = new ArrayList<>();
        List<BookInfoPojo> listOfBooksPojo = createdJsonListOfBooks;
        // Creating list to place authors, ratings and their number of occurences in rates in the books 
        ArrayList<String> listOfAuthorsNames = new ArrayList<>();
        ArrayList<Double> listOfAuthorsRatings = new ArrayList<>();
        ArrayList<Integer> listOfAuthorsOccurences = new ArrayList<>();

        for(int indexOfBook=0; listOfBooksPojo.size() > indexOfBook; indexOfBook++){
            if(!(listOfBooksPojo.get(indexOfBook).getAuthors()==null)){  
                
            // Adding first author if the list is empty, for usage in later for() with .size()
            if(listOfAuthorsNames.size()==0){
                listOfAuthorsNames.add(listOfBooksPojo.get(indexOfBook).getAuthors(0));
                listOfAuthorsRatings.add(0.0);
                listOfAuthorsOccurences.add(0);
                if(!(listOfBooksPojo.get(indexOfBook).getAverageRating()==null)){
                    // Adding rating if present, along with occurence with rating, to be later devided                    
                    double averageRatingOfAllBooks = listOfAuthorsRatings.get(0) + Double.parseDouble(listOfBooksPojo.get(indexOfBook).getAverageRating());
                    listOfAuthorsRatings.set(0, averageRatingOfAllBooks);
                    listOfAuthorsOccurences.set(0, 1);
                }
            }
                // Iterating through authors
                for(int indexOfAuthors=0; listOfBooksPojo.get(indexOfBook).getAuthors().size() > indexOfAuthors; indexOfAuthors++){
                    double averageRatingOfAllBooks = 0;
                    String authorName=listOfBooksPojo.get(indexOfBook).getAuthors(indexOfAuthors);
                    Integer indexOfAuthorInList=0;
                    
                    for(int indexOfListOfAuthorNames=0; listOfAuthorsNames.size() > indexOfListOfAuthorNames; indexOfListOfAuthorNames++){
                        // Iterating through array of authors in one bookInfoPojo, and checking if any of them is in the list of AuthorNames.
                        // If Not Present, adding him to it, if present checking if there is Average rating and adding it 
                        // to his place in ListOfAuthorsRatings and Occurenses.
                        if(!listOfAuthorsNames.contains(authorName)){
                            listOfAuthorsNames.add(authorName);
                            listOfAuthorsOccurences.add(0);
                            listOfAuthorsRatings.add(0.0);                                                      
                        }
                    }
                    
                    // Adding rating and occurences
                    if(!(listOfBooksPojo.get(indexOfBook).getAverageRating()==null)){
                        indexOfAuthorInList = listOfAuthorsNames.indexOf(authorName);
                        averageRatingOfAllBooks = listOfAuthorsRatings.get(indexOfAuthorInList) + Double.parseDouble(listOfBooksPojo.get(indexOfBook).getAverageRating());
                        listOfAuthorsRatings.set(indexOfAuthorInList, averageRatingOfAllBooks);
                        listOfAuthorsOccurences.set(indexOfAuthorInList, listOfAuthorsOccurences.get(indexOfAuthorInList)+1);
                   } 

            
            }
        }

    }

    // Calculating avarage rating by dividing all rates with number of occurences, and placing null inside a value if it didnt ocure at least 1 time
    // Null is used so that it is ignored when sending json
    for(int indexOfListOfAuthorNames=0; listOfAuthorsNames.size() > indexOfListOfAuthorNames; indexOfListOfAuthorNames++){
        AuthorsRatingPojo authorsRatingPojo = new AuthorsRatingPojo();
        authorsRatingPojo.setAuthor(listOfAuthorsNames.get(indexOfListOfAuthorNames));
        if(!(listOfAuthorsOccurences.get(indexOfListOfAuthorNames)== 0))
        authorsRatingPojo.setAverageRating(Double.toString(listOfAuthorsRatings.get(indexOfListOfAuthorNames)/listOfAuthorsOccurences.get(indexOfListOfAuthorNames)));
        else
        authorsRatingPojo.setAverageRating(null);
        sortedListOfAuthors.add(authorsRatingPojo);
        
    }

    // Sorting
    Collections.sort(sortedListOfAuthors,new ComparatorForAvarageRating().reversed());
    
    return sortedListOfAuthors;
        

    }

    // Custom comparator, since there are nulls present, if both values are null return equal,
    // if one of them is null return other as higher, and if both have a value compare
    public class ComparatorForAvarageRating implements Comparator<AuthorsRatingPojo> {
        @Override
        public int compare(AuthorsRatingPojo firstRating, AuthorsRatingPojo secondRating) {
            if(firstRating.getAverageRating()==null & secondRating.getAverageRating() == null)
            return 0;
            else{
            if(firstRating.getAverageRating()==null & !(secondRating.getAverageRating() == null))
            return -1;
            else{
                if(!(firstRating.getAverageRating()==null) & secondRating.getAverageRating() == null) 
                return 1;
                else{
                    return firstRating.getAverageRating().compareTo(secondRating.getAverageRating());
                }
            }
            }
            
            }
        }   

}
