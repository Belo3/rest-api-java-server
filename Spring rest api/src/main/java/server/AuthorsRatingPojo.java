package server;

import com.fasterxml.jackson.annotation.JsonInclude;


// Pojo for average rating of authors, and their names
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AuthorsRatingPojo{



    private String author;
    private String averageRating;


    public String getAuthor() {
        return author;
    }

 
    public String getAverageRating() {
        return averageRating;
    }


    public void setAuthor(String author) {
        this.author = author;
    }


    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }


}