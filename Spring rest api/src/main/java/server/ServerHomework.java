package server;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@EnableCaching
@SpringBootApplication
public class ServerHomework {

    public static void main(String[] args) {
      BookSearchFunctions bookSearchFunctions = new BookSearchFunctions();
      bookSearchFunctions.creatJsonList();
      SpringApplication app = new SpringApplication(ServerHomework.class);
      app.setDefaultProperties(Collections.singletonMap("server.port", "8080"));
      app.run(args);
    }
}