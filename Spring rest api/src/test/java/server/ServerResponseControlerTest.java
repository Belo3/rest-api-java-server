package server;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ServerResponseControlerTest {

    @Autowired
    private MockMvc mockMvc;

    // This tests use prepared data located in /JsonTestObcject
    // Since those test skip main, the addicional IF() had to be placed in all functions to check whether it is test or no, to use special data



    // Checks if api/book can properly find by Isbn, and return it in Json
    @Test
    public void checkForIsbn_13() throws Exception {
        BookSearchFunctions.ifThisIsTest=true;
        this.mockMvc.perform(get("/api/book/9781592432172")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("isbn").value("9781592432172"));
    }

    // Checks if api/book can properly find by ID, and return it in Json
    @Test
    public void checkForID() throws Exception {
        BookSearchFunctions.ifThisIsTest=true;
        this.mockMvc.perform(get("/api/book/gJEC2q7DzpQC")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("isbn").value("gJEC2q7DzpQC"));
    }


    // Checks if api/book can properly return 404 not found status
    @Test
    public void checkForHttpNotFound() throws Exception {
        BookSearchFunctions.ifThisIsTest=true;
        this.mockMvc.perform(get("/api/book/NoId")).andDo(print()).andExpect(status().isNotFound());
    }

    // Checks if api/category/books can find book with Religion and return it in Json
    @Test
    public void checkForCategoryReligion() throws Exception {
        BookSearchFunctions.ifThisIsTest=true;
        this.mockMvc.perform(get("/api/religion/books")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("The Religion of Java"));
    } 

    // Checks if api/category/books can find book with Java (Indonesia) and return it in Json
    @Test
    public void checkForCategoryJavaIndonesia() throws Exception {
        BookSearchFunctions.ifThisIsTest=true;
        this.mockMvc.perform(get("/api/Java (Indonesia)/books")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("The History of Java"));
    } 

    // Checks if api/search?queryToFind can find book with one word and return it in Json
    @Test
    public void checkForQueryInfoStrategist() throws Exception {
        BookSearchFunctions.ifThisIsTest=true;
        this.mockMvc.perform(get("/api/search?queryToFind=InfoStrategist")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].publisher").value("InfoStrategist.com"));
    }

    // Checks if api/search?queryToFind can find book with 2 words and marker in wrong order and return it in Json
    @Test
    public void checkForQueryTwoWords() throws Exception {
        BookSearchFunctions.ifThisIsTest=true;
        this.mockMvc.perform(get("/api/search?queryToFind=, this written")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("The Religion of Java"));
    }


    // Checks if api/rating can extract all the authors with thier rating
    @Test
    public void checkForAverageRating() throws Exception {
        BookSearchFunctions.ifThisIsTest=true;
        this.mockMvc.perform(get("/api/rating")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].author").value("Sir Thomas Stamford Raffles"))
                .andExpect(jsonPath("$[0].averageRating").value("4.5"))
                .andExpect(jsonPath("$[1].author").value("Clifford Geertz"))
                .andExpect(jsonPath("$[1].averageRating").value("4.0"));
    } 
    

}
